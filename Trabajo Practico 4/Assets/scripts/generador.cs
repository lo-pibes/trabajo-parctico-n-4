using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generador : MonoBehaviour
{
  
    private float tiempoinicial = 0;
    public float tiempomax = 1;
    public float altura;
    public GameObject columnas;


    void Start()
    {
        GameObject obstaculoNuevo = Instantiate(columnas);
        obstaculoNuevo.transform.position = transform.position + new Vector3(0,0,0);
        Destroy(gameObject, 100);
        
    }
    void Update()
    {
        if (tiempoinicial > tiempomax)
        {
            GameObject obstaculoNuevo = Instantiate(columnas);
            obstaculoNuevo.transform.position= transform.position + new Vector3(0, Random.Range(-altura, altura), 0);
            Destroy(gameObject, 100);
            tiempoinicial = 0;
        }
        else
        {
            tiempoinicial += Time.deltaTime;
        }
    }
    
}
